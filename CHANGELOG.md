## 1.75.2

Update to flutter Authenteq Flow 1.75.2

## 1.75.1

Update to flutter Authenteq Flow 1.75.1

## 1.75.0

Update to Authenteq Flow SDK 1.75.0

## 1.74.0

Update to Authenteq Flow SDK 1.74.0

## 1.73.0

Update to Authenteq Flow SDK 1.73.0

## 1.72.0

Update to Authenteq Flow SDK 1.72.0

## 1.71.0

Update to Authenteq Flow SDK 1.71.0

## 1.70.0

Update to Authenteq Flow SDK 1.70.0

For more details check https://docs.authenteq.com/
